2.15 State the order of evaluation of the operators in each of the following C++ statements and show the value of x after each statement is performed.

a. x = 7 + 3 * 6 / 2 - 1;
b. x = 2 % 2 + 2 * 2 - 2 / 2;
c. x = (3 * 9 * ( 3 + (9 * 3 / ( 3 ) ) ) );


Answers:


a. x = 7 + 3 * 6 / 2 - 1;

	1. 3 * 6 = 18;
	2. 18 / 2 = 9;
	3. 7 + 9 = 16;
	4. 16 - 1 = 15;
	5. x = 15;

b. x = 2 % 2 + 2 * 2 - 2 / 2;

	1. 2 % 2 = 0;
	2. 2 * 2 = 4;
	3. 2 / 2 = 1;
	4. 0 + 4 = 4;
	5. 4 - 1 = 3;
	6. x = 3;

c. x = (3 * 9 * ( 3 + (9 * 3 / ( 3 ) ) ) );

	1. 9 * 3 = 27;
	2. 27 / 3 = 9;
	3. 3 + 9 = 12;
	4. 3 * 9 = 27;
	5. 27 * 12 = 324;
	6. x = 324;
