State which of the following are true and which are false. If false, explain your answers.
a. C++ operators are evaluated from left to right.
b. The following are all valid variable names: _under_bar_ , m928134 , t5 , j7 , her_sales , his_account_total , a , b , c , z , z2 .
c. The statement cout << "a = 5;"; is a typical example of anassignment statement.
d. A valid C++ arithmetic expression with no parentheses is evaluated from left to right.
e. The following are all invalid variable names: 3g , 87 , 67h2 , h22 , 2h .

a. Wrong. C++ operators have a specific order of evaluation that is the same as in regular math.
b. True.
c. Wrong. That will just print a = 5; on the screen(a = 5; is the correct statement).
d. True.
e. Wrong. h22 is a valid vareiable name (Variable names can't start with a number but can contain them)