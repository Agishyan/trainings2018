Why might you want to write a program in a machine-independent language instead of a machine-dependent language? Why might a machine-dependent language be more appropriate for writing certain types of programs?



When one wants to write a cross-platform program that will run on systems with different hardware architecture, the machine-independent language will be preferred. Meanwhile, a machine-dependent language can be preferable when writing a program for a special system that will use the specific features of that system's hardware.
