Given the algebraic equation y = ax^3 + 7, which of the following, if any, are correct C++ statements for this equation? 
1.y = a * x * x * x + 7; CORRECT
2.y = a * x * x * ( x + 7 ); INCORRECT
3.y = ( a * x ) * x * ( x + 7 ); INCORRECT
4.y = (a * x) * x * x + 7; CORRECT
5.y = a * ( x * x * x ) + 7; CORRECT
6.y = a * x * ( x * x + 7 ); INCORRECT
